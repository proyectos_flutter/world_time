import 'package:http/http.dart';
import 'dart:convert';
import 'package:intl/intl.dart';

class WorldTime {

  late String location; // Locaion name for the UI
  late String time; // The time in that location
  late String flag; // url to an asset flag icon
  late String url; // Location url for api endpoint
  bool isDayTime = false; // true or false if daytime or not

  WorldTime({ required this.location, required this.flag, required this.url });

  Future<void> getTime() async {

    try {
       // String link = 'http://worldtimeapi.org/api/timezone/$url';
      // Uri urlapi = Uri.parse(link);

      // Make the request
      // Response response =   await get(urlapi);
      Response response =   await get(Uri.parse('http://worldtimeapi.org/api/timezone/$url'));
      // ignore: avoid_print
      Map data = jsonDecode(response.body);
    
      // ignore: avoid_print
      // print(data);

      // Get properties from data
      String datetime = data['datetime'];
      String offset = data['utc_offset'].substring(1,3);
      // ignore: avoid_print
      // print(datetime);
      // ignore: avoid_print
      // print(offset);

      // Create Dateme Objet
      DateTime now = DateTime.parse(datetime);
      now = now.add(Duration(hours: int.parse(offset)));

      // Set the time property
      isDayTime = now.hour > 6 && now.hour < 20 ? true : false;
      time = DateFormat.jm().format(now);
    }
    catch (e) {
      // ignore: avoid_print
      print('Caught error: $e');
      time = 'Could not get time data';
    }


   
    
  }
}



